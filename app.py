import cv2
import pytesseract

# Set Tesseract executable
pytesseract.pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"

# Read image
img = cv2.imread("examples/img2.png")

# Convert cv2's BGR to regular RGB
rgbImage = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


# Detect
print(pytesseract.image_to_string(rgbImage, lang="eng+rus"))


# Show
cv2.imshow('result', rgbImage)
cv2.waitKey(0)